/*
 * Copyright 2018 Timothy Sandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#include "confusion/examples/TagTracker.h"

namespace confusion {

template <typename StateType>
TagTracker<StateType>::TagTracker(ros::NodeHandle &node) : node_(node),
                                                imageTransport_(node), tagDetector_("tag36h11", 2),
                                                conFusor_(std::make_shared<StateType>(referenceFrameOffsets_)),
                                                statesBatch_(NUM_PROCESS_SENSORS, NUM_UPDATE_SENSORS),
                                                imuPropagator_(5000) {

  confusionPath_ = ros::package::getPath("confusion");
  configFile = confusionPath_ + configFile;
  std::string logFileName = confusionPath_ + "/data/tagtracker_log.txt";
  logger_ = std::unique_ptr<confusion::Logger>(new confusion::Logger(logFileName,
                                                                     *conFusor_.stateVector_.front()));

  boost::property_tree::read_info(configFile, pt);

  logData_ = pt.get<bool>("logData");

  conFusor_.setSolverOptions(pt.get<int>("numThreads"), pt.get<int>("maxNumIterations"));
  batchSize_ = pt.get<int>("batchSize");
  runBatch_ = pt.get<bool>("runBatch");
  forwardPropagateState_ = pt.get<bool>("forwardPropagateState");

  tagTrackerParameters_.twi_init_stddev = pt.get<double>("twi_init_stddev");
  tagTrackerParameters_.qwi_init_stddev = pt.get<double>("qwi_init_stddev");
  tagTrackerParameters_.vwi_init_stddev = pt.get<double>("vwi_init_stddev");
  tagTrackerParameters_.ba_init_stddev = pt.get<double>("ba_init_stddev");
  tagTrackerParameters_.bg_init_stddev = pt.get<double>("bg_init_stddev");
  tagTrackerParameters_.tagMeasCalibration_.ftr_init_stddev_ = pt.get<double>("ftr_init_stddev");
  tagTrackerParameters_.t_c_i_init_stddev = pt.get<double>("tci_init_stddev");
  tagTrackerParameters_.q_c_i_init_stddev = pt.get<double>("qci_init_stddev");

  //Read the tag map from the text file if desired
  if (pt.get<bool>("getTagMapFromFile")) {
    confusion::Pose<double> T_b_ib;
    confusion::Pose<double> T_ie_e;
    Eigen::Matrix<double, 4, 1> b_q;
    std::string tagMapFilename = confusionPath_ + "/examples/tagMap.txt";
    readTagPoses(tagMapFilename, referenceFrameOffsets_);

    //Add the tag poses to the static parameters
    Eigen::MatrixXd ftr_initialWeighting(2, 2);
    ftr_initialWeighting.setIdentity();
    ftr_initialWeighting /= pt.get<double>("ftr_init_stddev");
    Eigen::MatrixXd t_w_t_initialWeighting(3, 3);
    t_w_t_initialWeighting.setIdentity();
    t_w_t_initialWeighting /= pt.get<double>("twt_init_stddev");
    Eigen::MatrixXd q_w_t_initialWeighting(3, 3);
    q_w_t_initialWeighting.setIdentity();
    q_w_t_initialWeighting /= pt.get<double>("qwt_init_stddev");

    //Initialize the first tag, which is either completely fixed or has two rotational degrees of freedom
#ifdef OPT_GRAVITY
    conFusor_.staticParameters_.addParameter(confusion::Parameter(
            referenceFrameOffsets_.begin()->second->trans.data(), 3, referenceFrameOffsets_.begin()->first + "_trans", true));
    conFusor_.staticParameters_.addParameter(confusion::Parameter(
            referenceFrameOffsets_.begin()->second->rot.coeffs().data(), 4, referenceFrameOffsets_.begin()->first + "_rot", true,
            std::make_shared<confusion::QuatParam>()));
#else
    conFusor_.staticParameters_.addParameter(confusion::Parameter(
        referenceFrameOffsets_.begin()->second->trans.data(),
        3,
        referenceFrameOffsets_.begin()->first + "_trans",
        true));
    confusion::Parameter ftrParam(
        referenceFrameOffsets_.begin()->second->rot.coeffs().data(), 4, referenceFrameOffsets_.begin()->first + "_rot",
        std::make_shared<confusion::FixedYawParameterization>());
    ftrParam.setInitialConstraintWeighting(ftr_initialWeighting);
    conFusor_.staticParameters_.addParameter(ftrParam);
#endif

    //Initialize all other reference frames
    for (auto externalReferenceFrame: referenceFrameOffsets_) {
      confusion::Parameter
          tagTransParam(externalReferenceFrame.second->trans.data(), 3, externalReferenceFrame.first + "_trans");
      tagTransParam.setInitialConstraintWeighting(t_w_t_initialWeighting);
      confusion::Parameter tagRotParam(externalReferenceFrame.second->rot.coeffs().data(),
                                       4,
                                       externalReferenceFrame.first + "_rot",
                                       std::make_shared<confusion::QuatParam>());
      tagRotParam.setInitialConstraintWeighting(q_w_t_initialWeighting);
      conFusor_.staticParameters_.addParameter(tagTransParam);
      conFusor_.staticParameters_.addParameter(tagRotParam);
    }
  }

  //Set the initial constraint on the state
  std::vector<Eigen::MatrixXd> stateParamInitialWeightings(conFusor_.stateVector_.front()->parameters_.size());
  Eigen::MatrixXd param_weighting(3, 3);
  param_weighting.setIdentity();
  param_weighting /= tagTrackerParameters_.twi_init_stddev;
  stateParamInitialWeightings[0] = param_weighting;

  param_weighting.setIdentity();
  param_weighting /= tagTrackerParameters_.qwi_init_stddev;
  stateParamInitialWeightings[1] = param_weighting;

  param_weighting.setIdentity();
  param_weighting /= tagTrackerParameters_.vwi_init_stddev;
  stateParamInitialWeightings[2] = param_weighting;

  param_weighting.setIdentity();
  param_weighting /= tagTrackerParameters_.ba_init_stddev;
  stateParamInitialWeightings[3] = param_weighting;

  param_weighting.setIdentity();
  param_weighting /= tagTrackerParameters_.bg_init_stddev;
  stateParamInitialWeightings[4] = param_weighting;

  conFusor_.stateVector_.front()->setInitialStateWeighting(stateParamInitialWeightings);

  tagTrackerParameters_.tagMeasCalibration_.tagSize_ = pt.get<double>("tagSize");
  tagTrackerParameters_.imuCalibration_.gravityMagnitude_ = pt.get<double>("gravityMagnitude");
  tagTrackerParameters_.wi_stddev_ = pt.get<double>("wi_stddev");
  tagTrackerParameters_.ai_stddev_ = pt.get<double>("ai_stddev");
  tagTrackerParameters_.bg_stddev_ = pt.get<double>("bg_stddev");
  tagTrackerParameters_.ba_stddev_ = pt.get<double>("ba_stddev");
  tagTrackerParameters_.tag_corner_stddev_ = pt.get<double>("tag_corner_stddev");
  tagTrackerParameters_.initialize();

  gravity_rot_.setZero();
#ifdef OPT_GRAVITY
  //	Eigen::MatrixXd gravityRotInitialWeighting(2,2);
  //	gravityRotInitialWeighting.setIdentity();
  //	gravityRotInitialWeighting /= tagTrackerParameters_.tagMeasCalibration_.ftr_init_stddev;
      conFusor_.addStaticParameter(confusion::Parameter(gravity_rot_.data(), 2, "g_r")); //, gravityRotInitialWeighting);
#endif

  //todo How to dynamically set up X-many sensor frame offsets?
  //Initialize camera/imu offset
  auto T_c_i_ptr = std::make_shared<confusion::Pose<double>>();
  T_c_i_ptr->trans(0) = pt.get<double>("T_c_i.px");
  T_c_i_ptr->trans(1) = pt.get<double>("T_c_i.py");
  T_c_i_ptr->trans(2) = pt.get<double>("T_c_i.pz");
  T_c_i_ptr->rot.w() = pt.get<double>("T_c_i.qw");
  T_c_i_ptr->rot.x() = pt.get<double>("T_c_i.qx");
  T_c_i_ptr->rot.y() = pt.get<double>("T_c_i.qy");
  T_c_i_ptr->rot.z() = pt.get<double>("T_c_i.qz");
  T_c_i_ptr->rot.normalize();
  sensorFrameOffsets_["cam"] = T_c_i_ptr;

  if (pt.get<bool>("optimizeTci")) {
    //Optimize Tct
    Eigen::MatrixXd t_c_i_initialWeighting(3, 3);
    t_c_i_initialWeighting.setIdentity();
    t_c_i_initialWeighting /= tagTrackerParameters_.t_c_i_init_stddev;
    confusion::Parameter tciParam(T_c_i_ptr->trans.data(), 3, "T_c_i");
    tciParam.setInitialConstraintWeighting(t_c_i_initialWeighting);
    conFusor_.addStaticParameter(tciParam);
    Eigen::MatrixXd q_c_i_initialWeighting(3, 3);
    q_c_i_initialWeighting.setIdentity();
    q_c_i_initialWeighting /= tagTrackerParameters_.q_c_i_init_stddev;
    confusion::Parameter qciParam(T_c_i_ptr->rot.coeffs().data(), 4, "q_c_i", std::make_shared<confusion::QuatParam>());
    qciParam.setInitialConstraintWeighting(q_c_i_initialWeighting);
    conFusor_.addStaticParameter(qciParam);
  } else {
    //Don't optimize Tci
    conFusor_.addStaticParameter(confusion::Parameter(T_c_i_ptr->trans.data(), 3, "T_c_i", true));
    conFusor_.addStaticParameter(confusion::Parameter(T_c_i_ptr->rot.coeffs().data(),
                                                      4,
                                                      "q_c_i",
                                                      true,
                                                      std::make_shared<confusion::QuatParam>()));
  }

  //Set up publishers and subscribers
  auto callbackPtr = std::make_shared<confusion::SolverOverrunCallback>(newTagMeasReady_);
  conFusor_.setIterationCallback(callbackPtr);

  pubTagMarkers_ = node_.advertise<visualization_msgs::MarkerArray>("/tagMarkers", 10);
  pubStates_ = node_.advertise<geometry_msgs::PoseArray>("/states", 10);
  tagImagePub_ = imageTransport_.advertise("/detector_image", 1);
  pubState_ = node_.advertise<std_msgs::Float64MultiArray>("/pose_est_out", 10);
  pubRtState_ = node_.advertise<std_msgs::Float64MultiArray>("/rt_est_out", 10);
  pubPose_ = node_.advertise<geometry_msgs::PoseStamped>("/T_w_ie", 10);
  pubRtPose_ = node_.advertise<geometry_msgs::PoseStamped>("/T_w_ie_rt", 10);
  pubTagArray_ = node_.advertise<confusion::TagArray>("/tags", 10);

  //Start listening for the camera info message
  subCameraCalibration_ = node_.subscribe(pt.get<std::string>("camera_calibration_topic"), 10,
                                          &TagTracker<StateType>::camCalCallback, this);

  subTriggerBatch_ = node_.subscribe("/trigger_batch", 1, &TagTracker<StateType>::triggerBatchCalCallback, this);

  //Start listening for measurements
  subImu_ = node_.subscribe(pt.get<std::string>("imu_topic"), 1000, &TagTracker<StateType>::imuCallback,
                            this, ros::TransportHints().tcpNoDelay());

  //Start the estimator
  estimatorThread_ = std::thread(&TagTracker<StateType>::runEstimator, this);
  estimatorThread_.detach();

  std::cout << "[TagTracker] Initialization complete." << std::endl;
}

template <typename StateType>
TagTracker<StateType>::~TagTracker() {
  //Write tag poses to text file
  std::string tagMapFilename = confusionPath_ + "/examples/tagMap.txt";
  writeTagPoses(tagMapFilename, referenceFrameOffsets_);
}

template <typename StateType>
void TagTracker<StateType>::camCalCallback(const sensor_msgs::CameraInfo::ConstPtr &msg) {
  subCameraCalibration_.shutdown();

  if (firstMeasurement_) {
    t_epoch_ = msg->header.stamp.toSec();
    firstMeasurement_ = false;
  }

  //Set the projection matrix
  tagTrackerParameters_.tagMeasCalibration_.projMat_.setZero();
  tagTrackerParameters_.tagMeasCalibration_.projMat_(0, 0) = msg->P[0];
  tagTrackerParameters_.tagMeasCalibration_.projMat_(0, 2) = msg->P[2];
  tagTrackerParameters_.tagMeasCalibration_.projMat_(1, 1) = msg->P[5];
  tagTrackerParameters_.tagMeasCalibration_.projMat_(1, 2) = msg->P[6];
  tagTrackerParameters_.tagMeasCalibration_.projMat_(2, 2) = 1.0;
  std::cout << "projMat:\n" << tagTrackerParameters_.tagMeasCalibration_.projMat_ << std::endl;

  //Start listening for camera measurements
  subImage_ = imageTransport_.subscribe(pt.get<std::string>("camera_topic"), 2,
                                        &TagTracker<StateType>::imageCallback, this);
  subTagArray_ = node_.subscribe<confusion::TagArray>("camera0/tags",
                                                      2,
                                                      &TagTracker<StateType>::tagArrayCallback,
                                                      this,
                                                      ros::TransportHints().tcpNoDelay());

  std::cout << "Camera info received. Now listening for camera measurements." << std::endl;
}

template <typename StateType>
void TagTracker<StateType>::imuCallback(const sensor_msgs::Imu::ConstPtr &imuMsg) {
  if (!run_)
    return;

  if (firstMeasurement_) {
    t_epoch_ = imuMsg->header.stamp.toSec();
    firstMeasurement_ = false;
  }

  double t_imu_source = imuMsg->header.stamp.toSec() - t_epoch_;
  t_imu_latest_ = t_imu_source;

  Eigen::Vector3d a(imuMsg->linear_acceleration.x,
                    imuMsg->linear_acceleration.y,
                    imuMsg->linear_acceleration.z);
  Eigen::Vector3d w(imuMsg->angular_velocity.x,
                    imuMsg->angular_velocity.y,
                    imuMsg->angular_velocity.z);

  conFusor_.addProcessMeasurement(std::make_shared<confusion::ImuMeas>(
      t_imu_source, a, w, &tagTrackerParameters_.imuCalibration_, &gravity_rot_));

  //Optionally publish the forward-propagated state at IMU rate
  if (forwardPropagateState_) {
    confusion::ImuMeas meas(t_imu_source, a, w, &tagTrackerParameters_.imuCalibration_, &gravity_rot_);
    imuPropagator_.addImuMeasurement(meas);

    if (tracking_) {
      confusion::ImuStateParameters state = imuPropagator_.propagate(t_imu_source);

      geometry_msgs::PoseStamped pose_msg = getMsg(state.T_w_i_, "/world", ros::Time(state.t()));
      pubRtPose_.publish(pose_msg);

      std_msgs::Float64MultiArray msg;
      msg.layout.dim.resize(1);
      msg.layout.dim[0].label = "state";
      msg.layout.dim[0].size = 1;
      msg.layout.dim[0].stride = 21;
      msg.data.resize(21);
      msg.data[0] = state.t_ + t_epoch_;
      msg.data[1] = 0; //State index no longer used??
      msg.data[2] = state.T_w_i_.trans.x();
      msg.data[3] = state.T_w_i_.trans.y();
      msg.data[4] = state.T_w_i_.trans.z();
      msg.data[5] = state.T_w_i_.rot.w();
      msg.data[6] = state.T_w_i_.rot.x();
      msg.data[7] = state.T_w_i_.rot.y();
      msg.data[8] = state.T_w_i_.rot.z();
      msg.data[9] = state.angVel_.x();
      msg.data[10] = state.angVel_.y();
      msg.data[11] = state.angVel_.z();
      msg.data[12] = state.linVel_.x();
      msg.data[13] = state.linVel_.y();
      msg.data[14] = state.linVel_.z();
      msg.data[15] = state.accelBias_.x();
      msg.data[16] = state.accelBias_.y();
      msg.data[17] = state.accelBias_.z();
      msg.data[18] = state.gyroBias_.x();
      msg.data[19] = state.gyroBias_.y();
      msg.data[20] = state.gyroBias_.z();
      pubRtState_.publish(msg);

      //Publish tf message
      tfBroadcaster_.sendTransform(tf::StampedTransform(getTfMsg(state.T_w_i_), ros::Time::now(), "world", "imu"));
    }
  }

#ifdef SYNC_DEBUG
	std::cout << "Imu meas received with stamp " << t_imu_source << std::endl;
#endif
}

template <typename StateType>
void TagTracker<StateType>::processTagDetections(double t, std::vector<TagDetection> &tagDetections) {
  //Get the camera/imu offset pose
  std::shared_ptr<confusion::Pose<double>> T_c_i_ptr;
  try {
    T_c_i_ptr = sensorFrameOffsets_["cam"];
  }
  catch (const std::out_of_range &oor) {
    //todo Add new sensor offsets here dynamically? How to initialize them?
    std::cerr << "ERROR: T_c_i is not initialized! Throwing out the tag measurements." << std::endl;
    return;
  }

  for (int i = 0; i < tagDetections.size(); ++i) {
    std::array<std::array<double, 2>, 4> corners;
    corners[0][0] = tagDetections[i].corners[0][0];
    corners[0][1] = tagDetections[i].corners[0][1];
    corners[1][0] = tagDetections[i].corners[1][0];
    corners[1][1] = tagDetections[i].corners[1][1];
    corners[2][0] = tagDetections[i].corners[2][0];
    corners[2][1] = tagDetections[i].corners[2][1];
    corners[3][0] = tagDetections[i].corners[3][0];
    corners[3][1] = tagDetections[i].corners[3][1];

    std::string referenceFrameName = "tag" + std::to_string(tagDetections[i].id);
    conFusor_.addUpdateMeasurement(std::make_shared<confusion::TagMeas>(
        t - t_epoch_, referenceFrameName, *T_c_i_ptr, corners, tagTrackerParameters_.tagMeasCalibration_));

    newTagMeasReady_ = true;
  }

#ifdef SYNC_DEBUG
  std::cout << "Added " << tagDetections.size() << " tag measurements with stamp " << t-t_epoch_ << std::endl;
//	std::cout << "Latest IMU measurement is at " << t_imu_source-t_epoch << std::endl;
#endif
}

template <typename StateType>
void TagTracker<StateType>::tagArrayCallback(const confusion::TagArray::ConstPtr &msg) {
  if (!run_)
    return;

  if (!msg->tags.empty()) {
    std::vector<TagDetection> tagDetections;
    for (int i = 0; i < (msg->tags).size(); ++i) {
      tagDetections.push_back(TagDetection(msg->tags[i]));
    }
    processTagDetections(msg->header.stamp.toSec(), tagDetections);
  }
}

template <typename StateType>
void TagTracker<StateType>::imageCallback(const sensor_msgs::ImageConstPtr &msg) {
  if (!run_)
    return;

  cv_bridge::CvImagePtr cv_ptr;
  try {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
  } catch (cv_bridge::Exception &e) {
    ROS_ERROR("TagTracker<StateType>::imgaeCallback -- cv_bridge exception: %s", e.what());
    return;
  }

  //Detect tags in the image
  //todo Do this in another thread? Multithreaded tag detection?
  std::vector<TagDetection> tagDetections;
  tagDetector_.detectTags(cv_ptr->image, tagDetections);

  //Publish the image with the tags drawn on them
  cv_bridge::CvImagePtr out_cv_img(new cv_bridge::CvImage);
  out_cv_img->encoding = "rgb8";
  out_cv_img->header = msg->header;
  cv::cvtColor(cv_ptr->image, out_cv_img->image, CV_GRAY2RGB);
  tagDetector_.drawTags(out_cv_img->image, tagDetections);
  tagImagePub_.publish(out_cv_img->toImageMsg());

  if (!tagDetections.empty()) {
    //Publish a tagArray message for use in post-processing
    confusion::TagArray tagArrayMsg = getTagArrayMessage(msg->header.stamp, tagDetections);
    pubTagArray_.publish(tagArrayMsg);

    //Process the tag detections
    processTagDetections(msg->header.stamp.toSec(), tagDetections);
  }
}

template <typename StateType>
void TagTracker<StateType>::runEstimator() {
  ros::Rate loop_rate(loop_freq);

  while (run_) {
    if (!ros::ok()) {
      run_ = false;
      tracking_ = false;
      break;
    }

    ros::Time t_start = ros::Time::now();

    if (!conFusor_.assignMeasurements()) {
      loop_rate.sleep();
      continue; //No new states or measurements added, so wait for more measurements
    }
    newTagMeasReady_ = false;

    if (conFusor_.numStates() > batchSize_) {
      if (conFusor_.numStates() - batchSize_ > 1)
        std::cout << "WARNING: Had to marginalize " << conFusor_.numStates() - batchSize_ <<
                  " states in one iteration. The estimator is not running fast enough." << std::endl;
//      std::cout << "marginalizing " << conFusor_.numStates() - batchSize_ << " of " << conFusor_.numStates()
//                << " states. t_state_front=" << conFusor_.stateVector_.front()->t_ << std::endl;

      stateToDropIndex_ -= conFusor_.numStates() - batchSize_;

      //Optionally store the marginalized states to run a batch prboblem later
      if (runBatch_)
        conFusor_.marginalizeFrontStates(conFusor_.numStates() - batchSize_, &statesBatch_);
      else
        conFusor_.marginalizeFrontStates(conFusor_.numStates() - batchSize_);
    }

    //Remove the desired intermediate state
    //This is done here because the last Ceres problem has to be preserved until after marginalization
    if (stateToDropIndex_ > 0) {
      conFusor_.removeIntermediateState(stateToDropIndex_);
    }

    conFusor_.optimize();
    //conFusor_.briefPrint();

    //confusion::Pose<double> T_w_lidar;
    //try {
    //  T_w_lidar = *referenceFrameOffsets_.at("/map");
    //  T_w_lidar.print("T_w_lidar");
    //}
    //catch (const std::out_of_range &oor) {}
    //auto &T_imu_lidar_ptr = sensorFrameOffsets_["lidar"];
    //T_imu_lidar_ptr->print("T_imu_lidar");

    tracking_ = true;

//		ros::Time t_beforeDiagram = ros::Time::now();
//		confusion::Diagram diagram(conFusor_);
//		std::cout << "Drawing diagram took " << (ros::Time::now() - t_beforeDiagram).toSec() << " seconds\n";

//		controlBatchSize();

//		T_c_i_.print("T_c_i");
#ifdef OPT_GRAVITY
    std::cout << "gravity_rot: " << gravity_rot_.transpose() << std::endl;
#endif

    ros::Time t_end = ros::Time::now();
    double t_iter = (t_end - t_start).toSec();

    //std::cout << "Estimate delayed from latest IMU measurement by " << t_imu_latest_-conFusor_.stateVector_.back()->t() << " sec. Solver time: " << t_iter << " sec" << std::endl;
//    std::cout << "TagTracker run took " << t_iter << " sec\n" << std::endl;

    if (t_iter > 0.15)
      std::cout << "Long estimator iteration. It took " << t_iter << " sec" << std::endl;

    publish(conFusor_.stateVector_.back(), &(conFusor_.stateVector_));

    if (logData_) {
      Eigen::VectorXd priorResidual;
      std::vector<Eigen::VectorXd> processResiduals;
      std::vector<Eigen::VectorXd> updateResiduals;
      conFusor_.getResiduals(priorResidual, processResiduals, updateResiduals);

      logger_->writeBatch(conFusor_.stateVector_);
      logger_->writeStaticParameters(conFusor_.staticParameters_, conFusor_.stateVector_.back()->t_);
      logger_->writeResiduals(priorResidual, processResiduals, updateResiduals);

      //Print the residuals
      //std::cout << "Prior residual: " << priorResidual.transpose() << "\n";
      //for (auto &res: processResiduals)
      //  std::cout << "Process residual: " << res.transpose() << "\n";
      //for (auto &res: updateResiduals)
      //  std::cout << "Update residual: " << res.transpose() << "\n";
      //std::cout << std::endl;

      //todo Temp Additionally log the lidar update
//      if (!conFusor_.stateVector_.back()->updateMeasurements_[POSEMEAS].empty()) {
//        auto lidarMeas =
//            std::dynamic_pointer_cast<PoseMeas>(conFusor_.stateVector_.back()->updateMeasurements_[POSEMEAS].front());
//
//        if (lidarMeas) {
//          confusion::Pose<double> T_w_i_meas = lidarMeas->getMeasuredImuPose();
//          Eigen::VectorXd lidarMeasData(8);
//          lidarMeasData << lidarMeas->t(), T_w_i_meas.trans(0), T_w_i_meas.trans(1),T_w_i_meas.trans(2),
//              T_w_i_meas.rot.x(), T_w_i_meas.rot.y(), T_w_i_meas.rot.z(), T_w_i_meas.rot.w();
//          logger_->writeUserDataVector(lidarMeasData);
//        }
//      }
    }

    loop_rate.sleep();
  }

  runEstimatorLoopDone_ = true;
}

template <typename StateType>
void TagTracker<StateType>::stopTracking() {
  std::cout << "Stopping tracking" << std::endl;

  //Stop the estimator and wait for it to exit
  run_ = false;
  tracking_ = false;
  while (ros::ok() && !runEstimatorLoopDone_)
    ros::Rate(10).sleep();

  //Stop tracking so that it can be started with the optimized parameters
  conFusor_.stopTracking(&statesBatch_);

  runEstimatorLoopDone_ = false;
}

template <typename StateType>
void TagTracker<StateType>::startTracking() {
  if (run_) {
    std::cout << "TagTracker is already running." << std::endl;
    return;
  }

  stateToDropIndex_ = -1; //Careful. This holds some internal memory and needs to be reset.

  std::cout << "Starting tracking" << std::endl;
  run_ = true;
  estimatorThread_ = std::thread(&TagTracker<StateType>::runEstimator, this);
  estimatorThread_.detach();
}

template <typename StateType>
void TagTracker<StateType>::stopTracking(const std_msgs::Bool &msg) {
  stopTracking();
}

template <typename StateType>
void TagTracker<StateType>::startTracking(const std_msgs::Bool &msg) {
  startTracking();
}

template <typename StateType>
void TagTracker<StateType>::triggerBatchCalCallback(const std_msgs::Bool &msg) {
  if (!runBatch_) {
    std::cout << "WARNING: User requested a batch calibration run even runBatch is not set in "
                 "the config file. Cannot perform the batch calibration." << std::endl;
    return;
  }

  if (run_)
    stopTracking();

  std::cout << "Starting a batch calibration run for " << statesBatch_.size() << " states" << std::endl;

  //Activate the extrinsic calibrations
  auto &T_c_i_ptr = sensorFrameOffsets_["cam"];
  conFusor_.staticParameters_.getParameter(T_c_i_ptr->trans.data())->unsetConstant();
  conFusor_.staticParameters_.getParameter(T_c_i_ptr->rot.coeffs().data())->unsetConstant();

  //todo TEMP Don't use either IMU for the batch problem
//	for (int i=0; i<statesBatch_.size(); ++i) {
////		auto statePtr = std::dynamic_pointer_cast<ImuState>(statesBatch_[i]);
//		statesBatch_[i]->processChains_[IMU_E]->initialized_ = false;
//		statesBatch_[i]->processChains_[IMU_B]->initialized_ = false;
//	}

  //todo Be careful! I don't want to send this to SL! Separate the publishing functions?
  publish(statesBatch_.back(), &statesBatch_);

  confusion::BatchFusor batchFusor(statesBatch_, conFusor_.staticParameters_);
  batchFusor.buildProblem();

  Eigen::VectorXd priorResidual;
  priorResidual.resize(0);
  std::vector<Eigen::VectorXd> processResiduals;
  std::vector<Eigen::VectorXd> updateResiduals;
  batchFusor.getResiduals(processResiduals, updateResiduals);

  batchFusor.optimize();

  batchFusor.printParameterCovariance(T_c_i_ptr->trans.data(), "t_c_i");
  batchFusor.printParameterCovariance(T_c_i_ptr->rot.coeffs().data(), "q_c_i");

  //todo Be careful! I don't want to send this to SL! Separate the publishing functions?
  publish(statesBatch_.back(), &statesBatch_);

  T_c_i_ptr->print("T_c_i");
//conFusor_.stateVector_.front()->print();

  processResiduals.clear();
  updateResiduals.clear();
  batchFusor.getResiduals(processResiduals, updateResiduals);

  if (logData_) {
    logger_->writeBatch(batchFusor.stateVector_);
    logger_->writeStaticParameters(conFusor_.staticParameters_, statesBatch_.back()->t_);
    logger_->writeResiduals(priorResidual, processResiduals, updateResiduals);
  }

  //todo Calibration file for VI?
//	writePammCalibration(tagVector_, tagIdMap_, T_b_ib_, T_ie_e_, b_q_, gravity_rot_);

  //Reset the status of the extrinsic calibration parameters
  if (!pt.get<bool>("optimizeTci")) {
    conFusor_.staticParameters_.getParameter(T_c_i_ptr->trans.data())->setConstant();
    conFusor_.staticParameters_.getParameter(T_c_i_ptr->rot.coeffs().data())->setConstant();
  }

//	//Calibrate the Leica system
//	confusion::Pose<double> T_t0_w = tagVector_[tagIdMap_[72]]->T_w_t_.inverse();
//
//	std::string trajLogName = confusionPath_ + "/data/estimatedTrajectory.txt";
//	TrajectoryLog trajLog(trajLogName);
//
////	fitGroundTruthData(statesBatch_);
//	std::vector<double> timeVector(statesBatch_.size());
//	std::vector<confusion::Pose<double>> poseVector(statesBatch_.size());
//	for (int i=0; i<statesBatch_.size(); ++i) {
//		auto hyaState = std::dynamic_pointer_cast<ImuState>(statesBatch_[i]);
//		timeVector[i] = hyaState->t_ + t_epoch_ - t_offset;
//		poseVector[i] = T_t0_w * hyaState->T_w_i_;
//
//		trajLog.writeLine(hyaState->t_ + t_epoch_ - t_offset, T_t0_w * hyaState->T_w_i_);
//	}
//
//	fitGroundTruthData2(timeVector, poseVector, confusionPath_, confusion::Pose<double>()); //t_epoch_); //T_t0_w);

  //Empty the batch states to start logging for another batch problem
  statesBatch_.reset();

  //Now restart the estimator
  startTracking();
}

template <typename StateType>
void TagTracker<StateType>::publish(std::shared_ptr<confusion::State> statePtr, const confusion::StateVector *stateVector) {
  //Publish the most recent state for use in SL
  auto state = std::dynamic_pointer_cast<StateType>(statePtr);

  if (forwardPropagateState_) {
    imuPropagator_.update(state->getParameterStruct());
  }

  //todo Remove the unused entries from this message
  std_msgs::Float64MultiArray msg;
  msg.layout.dim.resize(1);
  msg.layout.dim[0].label = "state";
  msg.layout.dim[0].size = 1;
  msg.layout.dim[0].stride = 17;
  msg.data.resize(17);
  msg.data[0] = state->t_ + t_epoch_;
  msg.data[1] = state->T_w_i_.trans.x();
  msg.data[2] = state->T_w_i_.trans.y();
  msg.data[3] = state->T_w_i_.trans.z();
  msg.data[4] = state->T_w_i_.rot.w();
  msg.data[5] = state->T_w_i_.rot.x();
  msg.data[6] = state->T_w_i_.rot.y();
  msg.data[7] = state->T_w_i_.rot.z();
  msg.data[8] = state->linVel_.x();
  msg.data[9] = state->linVel_.y();
  msg.data[10] = state->linVel_.z();
  msg.data[11] = state->accelBias_.x();
  msg.data[12] = state->accelBias_.y();
  msg.data[13] = state->accelBias_.z();
  msg.data[14] = state->gyroBias_.x();
  msg.data[15] = state->gyroBias_.y();
  msg.data[16] = state->gyroBias_.z();
  pubState_.publish(msg);

  //Publish the latest T_w_i
  geometry_msgs::PoseStamped pose_msg;
  pose_msg.header.stamp = ros::Time(state->t_);
  pose_msg.header.frame_id = "world";
  pose_msg.pose.position.x = state->T_w_i_.trans.x();
  pose_msg.pose.position.y = state->T_w_i_.trans.y();
  pose_msg.pose.position.z = state->T_w_i_.trans.z();
  pose_msg.pose.orientation.w = state->T_w_i_.rot.w();
  pose_msg.pose.orientation.x = state->T_w_i_.rot.x();
  pose_msg.pose.orientation.y = state->T_w_i_.rot.y();
  pose_msg.pose.orientation.z = state->T_w_i_.rot.z();
  pubPose_.publish(pose_msg);

  //Publish a marker for each tag
  visualization_msgs::MarkerArray markerArray;
  visualization_msgs::Marker marker;
  marker.header.stamp = ros::Time::now();
  marker.header.frame_id = "/world";
  marker.type = visualization_msgs::Marker::CUBE;
  marker.action = visualization_msgs::Marker::MODIFY;
  marker.scale.x = 0.2;
  marker.scale.y = 0.2;
  marker.scale.z = 0.01;
  marker.color.a = 1.0; // Don't forget to set the alpha!
  marker.color.r = 1.0;
  marker.color.g = 1.0;
  marker.color.b = 1.0;
  int i = 0;
  for (auto &externalReferenceFrame: referenceFrameOffsets_) {
    std::string frameName = externalReferenceFrame.first;
    if (frameName.compare(0, 3, "tag") == 0) {
      marker.id = i;
      marker.pose = getMsg(*externalReferenceFrame.second);
      markerArray.markers.push_back(marker);
      ++i;
    }
  }
  pubTagMarkers_.publish(markerArray);

  //Publish an arrow for each state in the batch
  geometry_msgs::PoseArray statesMsg;
  statesMsg.header.frame_id = "/world";
  for (int i = 0; i < stateVector->size(); ++i) {
    auto state = std::dynamic_pointer_cast<const StateType>((*stateVector)[i]);
    statesMsg.poses.push_back(getMsg(state->T_w_i_));
  }
  pubStates_.publish(statesMsg);

  //Publish tfs for all active sensor and reference frames
  //todo Only supports one sensor offset for now
  ros::Time t_tf = ros::Time::now();
  for (auto &externalReferenceFrame: referenceFrameOffsets_) {
    tfBroadcaster_.sendTransform(tf::StampedTransform(getTfMsg(*externalReferenceFrame.second), t_tf,
                                                      "world", externalReferenceFrame.first));
  }

  publishDerived(statePtr, stateVector);
}

} // namespace confusion
