/*!
@page installation Installation
ConFusion is written in C++. It is tested running in Ubuntu 14.04 and 16.04.

\section dependencies Dependencies
First install the following dependencies:
 - <a href="http://wiki.ros.org/ROS/Installation" target="_blank">ROS</a>. ConFusion is tested with releases Indigo and Kinetic. (soon optional...)
 - <a href="http://eigen.tuxfamily.org/index.php?title=Main_Page" target="_blank">Eigen 3</a>
 - <a href="http://wiki.ros.org/catkin" target="_blank">catkin</a> build system
 - <a href="http://ceres-solver.org/installation.html" target="_blank">Ceres Solver</a>. We recommend installing the optional SuiteSparse when running large sensor fusion problems.
 - <a href="https://opencv.org" target="_blank">OpenCV</a>. Both OpenCV2 and OpenCV3 are supported. (soon optional...)
 
\section compilation Compilation
 To build ConFusion using 
 <a href="https://catkin-tools.readthedocs.io/en/latest/" target="_blank">catkin_tools</a>, 
 execute the following commands in a terminal 
 (assuming that your catkin workspace is named "catkin_ws"):
 \verbatim
 cd catkin_ws/src
 git clone https://bitbucket.org/tsandy/confusion.git
 catkin build -DCMAKE_BUILD_TYPE=RELEASE
 \endverbatim
 
 To build this documentation:
 \verbatim
 cd catkin_ws/src/confusion
 doxygen Doxyfile
 \endverbatim
 
 To run the unit tests:
 \verbatim
 cd catkin_ws/src/confusion
 catkin run_tests --this
 \endverbatim
 
\section ucf Using ConFusion
 To start building your own sensor fusion problem, create a separate package which
 depends on ConFusion and refer to \ref getting_started "getting started".
*/